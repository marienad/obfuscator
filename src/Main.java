import java.io.IOException;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        String pathToFile = scanner.nextLine();
        MyFile file = new MyFile(pathToFile);
        try {
            file.obfuscate();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("ready");
    }
}
