import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyFile {
    private String pathToFile;
    private String name;
    private String newName;
    private List<String> keys;
    private String code;

    public MyFile(String pathToFile) throws IOException {
        test();
        this.pathToFile = pathToFile;
        String s = new File(pathToFile).getName();
        name = s.substring(0, s.length() - 5);
        newName = "A";
    }

    /**
     * Обфусцирует java код в файле
     */
    void obfuscate() throws IOException {
        List<String> file = Files.readAllLines(Paths.get(pathToFile));
        deleteNew(file);
        deleteWhite();
        deleteComment();
        changeFile();
        renameClass();
        renameIdentifier();
        Files.write(Paths.get(pathToFile), Collections.singleton(code));
    }

    /**
     * Проверяет нужного ли формала файл
     *
     * @throws IOException если файл не java
     */
    private void test() throws IOException {
        if (!pathToFile.contains(".java")) {
            throw new IOException("not Java file");
        }
    }

    /**
     * Возвращает код с измененными идентификаторами
     */
    private void renameIdentifier() throws IOException {
        List<String> names = findNames();
        char j = 'a';
        char k = 'a';
        for (int i = 0; i < names.size(); i++) {
            code = code.replaceAll(names.get(i), String.valueOf(j) + String.valueOf(k));
            k++;
            if (i % 26 == 0) {
                j++;
                k = 'a';
            }
        }
    }

    /**
     * Возвращает список всех идентификаторов
     *
     * @return список идентификаторов
     */
    private List<String> findNames() throws IOException {
        fill();
        String s = "(?<= )[a-z]\\w+(?= )|(?<= )[a-z]\\w+(?=\\()";
        Pattern pattern = Pattern.compile(s);
        Matcher matcher = pattern.matcher(code);
        List<String> list = new ArrayList<>();
        while (matcher.find()) {
            if (!keys.contains(matcher.group())) {
                list.add(matcher.group());
            }
        }
        return list;
    }

    /**
     * Заполняет список ключевых слов
     */
    private void fill() throws IOException {
        keys = Files.readAllLines(Paths.get("D:\\Work\\Obfuscator\\src\\keys.txt"));
    }

    /**
     * Создает новый файл
     */
    private void changeFile() throws IOException {
        //   new File(pathToFile).delete();
        pathToFile = pathToFile.replaceAll(name, newName);
        Files.write(Paths.get(pathToFile), Collections.singleton(code));
    }

    /**
     * Меняет имя класса
     */
    private void renameClass() throws IOException {
        code = code.replaceAll(name, newName);
    }

    /**
     * Удаляет переоды на новую строку
     *
     * @param file список строк
     */
    private void deleteNew(List<String> file) {
        code = "";
        for (int i = 0; i < file.size(); i++) {
            code += file.get(i) + " ";
        }
    }

    /**
     * Удаляет комментарии
     */
    private void deleteComment() {
        code = code.replaceAll("(?s:/\\*.*?\\*/)|//.*", "");
    }

    /**
     * Удаляет пробелы
     */
    private void deleteWhite() {
        code = code.replaceAll("\\s+", " ").trim();
    }
}
